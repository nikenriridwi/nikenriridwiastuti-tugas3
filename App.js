import * as React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from './Home';
import Netinfo from './Netinfo';
import DateTime from './DateTimePicker';
import SliderApp from './Slider';
import GeoApp from './Geo';
import ProgressApp from './Progress';
import BarApp from './ProgressBar';
import ClipboardApp from './Clipboard';
import AsyncApp from './AsyncApp';
import CarouselApp from './CarouselApp';
import ZoomViewApp from './ZoomView';
import LinearApp from './linear';
import RenderApp from './Render';
import ShareApp from './Share';
import SkeletonApp from './Skeleton';
import WebViewApp from './WebView';
import TooltipApp from './Tooltip';
import VectorIconApp from './VectorIcon';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Net Info" component={Netinfo} />
        <Stack.Screen name="Date Time" component={DateTime} />
        <Stack.Screen name="Slider" component={SliderApp} />
        <Stack.Screen name="Geolocation" component={GeoApp} />
        <Stack.Screen name="Progress View" component={ProgressApp} />
        <Stack.Screen name="Progress Bar" component={BarApp} />
        <Stack.Screen name="Clipboard" component={ClipboardApp} />
        <Stack.Screen name="Async Storage" component={AsyncApp} />
        <Stack.Screen name="Carousel" component={CarouselApp} />
        <Stack.Screen name="Zoom Viewer" component={ZoomViewApp} />
        <Stack.Screen name="Linear Gradient" component={LinearApp} />
        <Stack.Screen name="Render HTML" component={RenderApp} />
        <Stack.Screen name="Share" component={ShareApp} />
        <Stack.Screen name="Skeleton" component={SkeletonApp} />
        <Stack.Screen name="Web View" component={WebViewApp} />
        <Stack.Screen name="Tooltip" component={TooltipApp} />
        <Stack.Screen name="Vector Icon" component={VectorIconApp} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
