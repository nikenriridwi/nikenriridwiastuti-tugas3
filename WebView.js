import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Image,
} from 'react-native';
import WebView from 'react-native-webview';

const YOUTUBE = 'https://www.youtube.com/';
const GOOGLE = 'https://www.google.com/';

const WebViewApp = () => {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={{width: '100%', height: '100%'}}>
        <WebView
          source={{uri: YOUTUBE}}
          onLoad={console.log('Loaded!')}></WebView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 28,
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default WebViewApp;
