import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Modal,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const LinearApp = () => {
  return (
    <View
      style={{
        height: 400,
        width: '100%',
        justifyContent: 'center',
        alignContent: 'center',
      }}>
      <TouchableOpacity
        style={{
          height: 50,
          width: '60%',
          backgroundColor: 'skyblue',
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 20,
        }}>
        <Text>Press Me</Text>
      </TouchableOpacity>

      <LinearGradient
        colors={['#000', '#fff', '#192f6a']}
        style={{
          borderRadius: 20,
          marginTop: 50,
          height: 50,
          width: '60%',
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: 15, color: '#000'}}>Sign in with facebook</Text>
      </LinearGradient>
    </View>
  );
};

export default LinearApp;
