import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  Modal,
  StatusBar,
  Button,
  PermissionsAndroid,
  Linking,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';

const GeoApp = ({navigation}) => {
  const Permission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Cool Photo App Location Permission',
          message: 'Cool Photo App needs access to your Location ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Location');
        getCurrentLocation();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  const [currentLocation, setCurrentLocation] = useState(null);

  const getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        setCurrentLocation({latitude, longitude});
        console.log(latitude, longitude);
      },
      error => alert('Error', error.message),
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  };

  const openMaps = () => {
    const {latitude, longitude} = currentLocation;
    if ((latitude, longitude)) {
      const url = `https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`;
      Linking.openURL(url);
    } else {
      alert('Location not available');
    }
  };

  return (
    <View>
      <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>
        Get Coords
      </Text>
      <View
        style={{
          backgroundColor: 'white',
          padding: 10,
          margin: 10,
          alignItems: 'center',
        }}>
        <Text>
          Latitude: {currentLocation ? currentLocation.latitude : 'Loading...'}
        </Text>
        <Text>
          Longitude:{' '}
          {currentLocation ? currentLocation.longitude : 'Loading...'}
        </Text>
      </View>

      {currentLocation ? (
        <>
          <TouchableOpacity onPress={openMaps}>
            <View
              style={{
                backgroundColor: 'blue',
                padding: 10,
                alignItems: 'center',
                margin: 10,
              }}>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                Open Maps
              </Text>
            </View>
          </TouchableOpacity>
        </>
      ) : (
        <>
          <TouchableOpacity onPress={Permission}>
            <View
              style={{
                backgroundColor: 'red',
                padding: 10,
                alignItems: 'center',
                margin: 10,
              }}>
              <Text>Get Location</Text>
            </View>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default GeoApp;
