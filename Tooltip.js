import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Image,
} from 'react-native';
import Tooltip from 'rn-tooltip';

const TooltipApp = () => {
  return (
    <View style={styles.container}>
      <Tooltip popover={<Text>Hii There!</Text>}>
        <Text>Press Me</Text>
      </Tooltip>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default TooltipApp;
