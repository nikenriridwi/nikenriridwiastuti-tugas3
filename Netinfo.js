import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  Button,
} from 'react-native';

import NetInfo from '@react-native-community/netinfo';

const Netinfo = ({navigation}) => {
  const [netInfo, setNetInfo] = useState('');

  useEffect(() => {
    const data = NetInfo.addEventListener(state => {
      setNetInfo(
        `connectionType:${state.type} isConnected?: ${state.isConnected}`,
      );
    });

    return () => {
      data();
    };
  }, []);

  const handleGetNetInfo = () => {
    NetInfo.fetch().then(state => {
      console.log(state);
      alert(`connectionType:${state.type} isConnected?: ${state.isConnected}`);
    });
  };
  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={{padding: 10, color: 'black', fontSize: 16}}>
          {netInfo}
        </Text>
        <TouchableOpacity>
          <Button
            title="Get Net Information"
            onPress={() => {
              handleGetNetInfo();
            }}></Button>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 300,
  },
});

export default Netinfo;
