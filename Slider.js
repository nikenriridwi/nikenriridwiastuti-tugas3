import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  Modal,
  StatusBar,
  Button,
} from 'react-native';
import Slider from '@react-native-community/slider';

const SliderApp = ({navigation}) => {
  const [range, setRange] = useState('50%');
  const [sliding, setSliding] = useState('inactive');

  return (
    <View style={styles.container}>
      <Text style={{fontSize: 20, fontWeight: 'bold'}}>{range}</Text>
      <Text style={{fontSize: 20, fontWeight: 'bold'}}>{sliding}</Text>
      <StatusBar style="auto"></StatusBar>

      <Slider
        style={{width: 250, height: 40}}
        minimumValue={0}
        maximumValue={1}
        minimumTrackTintColor="red"
        maximumTrackTintColor="black"
        thumbTintColor="red"
        value={0.5}
        onValueChange={value => setRange(parseInt(value * 100) + '%')}
        onSlidingStart={() => setSliding('Sliding')}
        onSlidingComplete={() => setSliding('Inactive')}></Slider>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SliderApp;
