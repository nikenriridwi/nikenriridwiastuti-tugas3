import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

const AsyncApp = () => {
  const [TextInputValue, setTextInputValue] = useState('');
  const [value, setValue] = useState('');

  const saveValue = () => {
    if (TextInputValue) {
      AsyncStorage.setItem('any_key_here', TextInputValue);
      setTextInputValue('');
      alert('Data saved');
    } else {
      alert('Please input data');
    }
  };

  const getValue = () => {
    AsyncStorage.getItem('any_key_here').then(value => {
      setValue(value);
    });
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <Text style={styles.tittle}>AsyncStorage in React Native</Text>
        <TextInput
          placeholder="Insert some text here"
          value={TextInputValue}
          onChangeText={data => setTextInputValue(data)}
          underlineColorAndroid="transparant"
          style={styles.TextInputStyle}></TextInput>

        <TouchableOpacity onPress={saveValue} style={styles.button}>
          <Text style={styles.buttonText}>Save Value</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={getValue} style={styles.button}>
          <Text style={styles.buttonText}>Get Value</Text>
        </TouchableOpacity>

        <Text style={styles.text}>{value}</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'white',
  },
  tittle: {
    color: 'black',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
  },
  TextInputStyle: {
    textAlign: 'center',
    height: 60,
    width: '100%',
    borderWidth: 1,
    borderColor: 'blue',
    fontSize: 22,
  },
  button: {
    fontSize: 16,
    color: 'white',
    backgroundColor: 'blue',
    padding: 5,
    marginTop: 10,
    minWidth: 250,
    height: 60,
    justifyContent: 'center',
  },
  buttonText: {
    padding: 5,
    color: 'white',
    textAlign: 'center',
    fontSize: 22,
  },
  text: {
    padding: 10,
    textAlign: 'center',
  },
});

export default AsyncApp;
