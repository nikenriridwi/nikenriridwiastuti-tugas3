import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  Modal,
  StatusBar,
  Button,
  PermissionsAndroid,
  Linking,
} from 'react-native';
import {ProgressView} from '@react-native-community/progress-view';

const ProgressApp = () => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    //simulate progress increment
    const interval = setInterval(() => {
      if (progress < 1) {
        setProgress(progress + 0.1);
      } else if (progress > 1) {
        clearInterval(interval);
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [progress]);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Progress View Application</Text>
      <ProgressView
        style={{width: 200}}
        progress={progress}
        progressTintColor="blue"></ProgressView>
      <Text style={styles.text}>{Math.round(progress * 100)}%</Text>
      <Button
        title="Reset the progress"
        onPress={() => setProgress(0)}
        color="red"></Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 20,
    marginBottom: 10,
  },
  text: {
    marginTop: 10,
    fontSize: 18,
  },
});

export default ProgressApp;
