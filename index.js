/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import Netinfo from './Netinfo';
// import DateTime from './DateTimePicker';
// import SliderApp from './Slider';
// import GeoApp from './Geo';
// import ProgressApp from './Progress';
// import BarApp from './ProgressBar';
// import ClipboardApp from './Clipboard';
// import AsyncApp from './AsyncApp';
// import CarouselApp from './CarouselApp';
// import ZoomViewApp from './ZoomView';
// import LinearApp from './linear';
// import RenderApp from './Render';
// import ShareApp from './Share';
// import SkeletonApp from './Skeleton';
// import WebViewApp from './WebView';
// import TooltipApp from './Tooltip';
// import VectorIconApp from './VectorIcon';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
