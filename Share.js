import {View, Text, Button} from 'react-native';
import React from 'react';
import Share from 'react-native-share';

const ShareApp = () => {
  const share = async () => {
    const options = {
      message: 'Hii there this is Niken Riri',
      url: 'https://instagram.com/nikenriri__',
      email: 'nikenriri05@gmail.com',
      subject: 'Lets keep in touch and having a good friendship',
      recipient: '+62808997867',
    };

    try {
      const res = await Share.open(options);
      console.log(res);
    } catch (err) {
      console.log(err);
    }

    // Share.open(options)
    //   .then(res => console.log(res))
    //   .catch(err => console.log(err));
  };

  return (
    <View>
      <Text
        style={{
          fontSize: 40,
          marginVertical: 40,
          color: 'black',
          textAlign: 'center',
        }}>
        React Native Share
      </Text>
      <View style={{marginHorizontal: 40}}>
        <Button title="Share" onPress={share} />
      </View>
    </View>
  );
};

export default ShareApp;
