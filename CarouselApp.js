import React, {useEffect, useRef, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Button,
  Dimensions,
  Image,
  StatusBar,
} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';

export const SLIDER_WIDTH = Dimensions.get('window').width + 30;
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.8);
const data = [
  {
    id: 1,
    name: 'Slide 1',
    url: 'https://th.bing.com/th/id/R.e8b9eb5c1f8bd228a1a388781247cf3e?rik=pEEci4dSf0lbow&riu=http%3a%2f%2fwww.wired.com%2fwp-content%2fuploads%2f2014%2f10%2fff_disneybaymax10_g.jpeg&ehk=oB6U9wBJY88L7y7F%2bKeIQjsFVOdN8NtLw5Oqlist65s%3d&risl=&pid=ImgRaw&r=0',
  },
  {
    id: 2,
    name: 'Slide 2',
    url: 'https://th.bing.com/th/id/R.e8b9eb5c1f8bd228a1a388781247cf3e?rik=pEEci4dSf0lbow&riu=http%3a%2f%2fwww.wired.com%2fwp-content%2fuploads%2f2014%2f10%2fff_disneybaymax10_g.jpeg&ehk=oB6U9wBJY88L7y7F%2bKeIQjsFVOdN8NtLw5Oqlist65s%3d&risl=&pid=ImgRaw&r=0',
  },
  {
    id: 3,
    name: 'Slide 3',
    url: 'https://th.bing.com/th/id/R.e8b9eb5c1f8bd228a1a388781247cf3e?rik=pEEci4dSf0lbow&riu=http%3a%2f%2fwww.wired.com%2fwp-content%2fuploads%2f2014%2f10%2fff_disneybaymax10_g.jpeg&ehk=oB6U9wBJY88L7y7F%2bKeIQjsFVOdN8NtLw5Oqlist65s%3d&risl=&pid=ImgRaw&r=0',
  },
  {
    id: 4,
    name: 'Slide 4',
    url: 'https://th.bing.com/th/id/R.e8b9eb5c1f8bd228a1a388781247cf3e?rik=pEEci4dSf0lbow&riu=http%3a%2f%2fwww.wired.com%2fwp-content%2fuploads%2f2014%2f10%2fff_disneybaymax10_g.jpeg&ehk=oB6U9wBJY88L7y7F%2bKeIQjsFVOdN8NtLw5Oqlist65s%3d&risl=&pid=ImgRaw&r=0',
  },
];

const renderItem = ({item}) => {
  return (
    <View>
      <Image source={{uri: item.url}} style={{width: 200, height: 200}}></Image>
      <Text style={{marginVertical: 10, fontSize: 20, fontWeight: 'bold'}}>
        {item.name}
      </Text>
    </View>
  );
};

const CarouselApp = () => {
  const [index, setIndex] = useState(0);
  const isCarousel = useRef(null);

  return (
    <View style={{paddingTop: 200, alignItems: 'center'}}>
      <Carousel
        ref={isCarousel}
        data={data}
        renderItem={renderItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        onSnapToItem={index => setIndex(index)}></Carousel>
      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 10,
          marginHorizontal: 8,
          backgroundColor: 'green',
        }}></Pagination>
    </View>
  );
};

//const styles = StyleSheet.create({});

export default CarouselApp;
