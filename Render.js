import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Image,
  useWindowDimensions,
} from 'react-native';
import RenderHTML from 'react-native-render-html';

const source = {
  html: `
    <header class="bg-transparent fixed z-[9999] bg-white bg-opacity-70 top-0 left-0 w-full flex items-center z-10">
    <div class="container">
      <div class="flex item-center justify-between relative">
        <div class="px-4">
          <a href="#home" class="font-bold text-lg text-cyan-500 block py-6">Niken Riri</a>
        </div>
        <div class="flex items-center px-4">
          <button  id="hamburger" name="hamburger" type="button" class="block absolute right-4 lg:hidden">
            <span class="w-[30px] h-[2px] my-2 block bg-slate-900"></span>
            <span class="w-[30px] h-[2px] my-2 block bg-slate-900"></span>
            <span class="w-[30px] h-[2px] my-2 block bg-slate-900"></span>
          </button>

          <nav id="nav-menu" class="hidden absolute py-5 bg-white shadow-lg rounded-lg max-w-[250px] w-full right-4 top-full lg:block lg:static lg:bg-transparent lg:max-w-full lg:shadow-none lg:rounded-none">
            <ul class="block lg:flex">
              <li class="group">
                <a href="#home" class="text-base text-slate-900 py-2 mx-8 flex group-hover:text-cyan-500">Beranda</a>
              </li>
              <li class="group">
                <a href="#pendidikan" class="text-base text-slate-900 py-2 mx-8 flex group-hover:text-cyan-500">Pendidikan</a>
              </li>
              <li class="group">
                <a href="#porto" class="text-base text-slate-900 py-2 mx-8 flex group-hover:text-cyan-500">Portfolio</a>
              </li>
              <li class="group">
                <a href="#contact" class="text-base text-slate-900 py-2 mx-8 flex group-hover:text-cyan-500">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>`,
};

const RenderApp = () => {
  const {width} = useWindowDimensions();
  return <RenderHTML contentWidth={width} source={source} />;
};

export default RenderApp;
