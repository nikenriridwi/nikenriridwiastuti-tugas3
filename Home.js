import React from 'react';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.ScrollViewStyle}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Net Info')}>
          <Text style={styles.text}>Net Info</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Date Time')}>
          <Text style={styles.text}>Date Time</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Slider')}>
          <Text style={styles.text}>Slider</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Geolocation')}>
          <Text style={styles.text}>Geolocation</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Progress View')}>
          <Text style={styles.text}>Progress View</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Progress Bar')}>
          <Text style={styles.text}>Progress Bar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Clipboard')}>
          <Text style={styles.text}>Clipboard</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Async Storage')}>
          <Text style={styles.text}>Async Storage</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Carousel')}>
          <Text style={styles.text}>Carousel</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Zoom Viewer')}>
          <Text style={styles.text}>Zoom Viewer</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Linear Gradient')}>
          <Text style={styles.text}>Linear Gradient</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Render HTML')}>
          <Text style={styles.text}>Render HTML</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Share')}>
          <Text style={styles.text}>Share</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Skeleton')}>
          <Text style={styles.text}>Skeleton</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Web View')}>
          <Text style={styles.text}>Web View</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Tooltip')}>
          <Text style={styles.text}>Tooltip</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Vector Icon')}>
          <Text style={styles.text}>Vector Icon</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ScrollViewStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'aqua',
    fontSize: 12,
    width: '60%',
    height: 43,
    marginVertical: 10,
    padding: 10,
    borderRadius: 20,
  },
  text: {
    color: 'White',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
  },
});

export default HomeScreen;
