import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Image,
  Modal,
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

const images = [
  {
    url: 'https://4.bp.blogspot.com/-YcJK3hoyQXM/V2yAG68U5vI/AAAAAAAAFWM/hMGdb9nkNN4REKhcUHF0oHzZPiuhYszsACLcB/s1600/Wallpaper%2BKucing%2BImut%2BLucu%2B-%2BKartunlucu.Com.jpg',
  },
  {
    url: 'https://media3.cgtrader.com/variants/qyp4CNNCxeTHFkMwtu8j4qPG/40073f86dea5cc27b3e46b911284f10ff35833da74046da55f55f229c8993de7/1.jpg',
  },
  {
    url: 'https://th.bing.com/th/id/OIP.IifsWXldN0QIVdFOMoQuiQHaHZ?pid=ImgDet&rs=1',
  },
  {
    url: 'https://dewipuspasari.files.wordpress.com/2022/07/images-2022-07-23t232701.358.jpeg',
  },
];

const ZoomViewApp = () => {
  return (
    <Modal visible={true} transparent={true}>
      <ImageViewer imageUrls={images}></ImageViewer>
    </Modal>
  );
};

export default ZoomViewApp;
