import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const VectorIconApp = () => {
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Icon name="rocket" size={30} color="blue" />
        <Icon name="ghost" size={30} color="blue" />
        <Icon name="gift" size={30} color="blue" />
      </View>
      <Text style={styles.text}>Hello, React Native with Vector Icons!</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    marginTop: 10,
  },
});

export default VectorIconApp;
